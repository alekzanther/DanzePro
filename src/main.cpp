#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "danzepro.h"
//#include "kirigami/src/kirigamiplugin.h"
using namespace DanzeProApp;



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<DanzePro>("danzepro.model", 1, 0, "DanzePro");
    qmlRegisterType<DanzePlaylist>("danzepro.playlist", 1, 0, "DanzePlaylist");
    qmlRegisterType<DanzeSong>("danzepro.song", 1, 0, "DanzeSong");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    foreach (auto i, app.allWindows()) {
        i->setIcon(QIcon(":/logo.png"));
    }

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
