#ifndef DANZEPRO_H
#define DANZEPRO_H

#include <QtCore>
#include <QtGui>
#include <QMediaPlayer>
#include <QSettings>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlAutoPropertyHelpers.h"
#include "QQmlObjectListModel.h"
#include "QQmlVariantListModel.h"

#include "danzeplaylist.h"

#include "downloadmanager.h"



namespace DanzeProApp {
class DanzePlaylist;

class DanzePro : public QObject
{
    Q_OBJECT
    QML_OBJMODEL_PROPERTY(DanzePlaylist, playlists)
    QML_WRITABLE_PTR_PROPERTY(DanzePlaylist, active_playlist)
    QML_WRITABLE_PTR_PROPERTY(DanzeSong, active_song)
public:
    explicit DanzePro(QObject *parent = Q_NULLPTR);
    ~DanzePro();
    Q_INVOKABLE void addPlaylist(QString name, QSettings *settings = Q_NULLPTR);
    Q_INVOKABLE void setPlaylist(DanzePlaylist *pl);
    Q_INVOKABLE void loadSettings();
    Q_INVOKABLE void saveSettings();
public slots:

    void songPlaying(DanzeSong *song);
    void spacePressed();
private:
    QMediaPlayer *player;
    friend class DanzePlaylist;
    DownloadManager *dlManager;
};

}
#endif // DANZEPRO_H
