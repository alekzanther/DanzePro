import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: root
    width: ListView.view.width
    height: 68
    property alias text: label.text
    property alias textColor: label.color

    signal clicked()
    signal remove()

    Rectangle {
        anchors.fill: parent
        color: "transparent"
        opacity: 0
        border.color: Qt.darker(color)
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton

            onClicked: {
                if (mouse.button === Qt.RightButton)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }

            onPressAndHold: {
                if (mouse.source === Qt.MouseEventNotSynthesized)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }


            Menu {
                id: contextMenu
                MenuItemWithMDI {
                    text: "Delete"
                    mdiIcon: '\uf5e8'
                    mdiIconColor: "red"
                    onTriggered: {
                        root.remove();
                    }
                }

            }
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: 8
        anchors.rightMargin: 8
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Text {
                id: label
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 8
                color: '#f7f7f7'
                font.pixelSize: 24
            }
            MouseArea {
                anchors.fill: parent
                onClicked: root.clicked()
            }
        }

    }
}
