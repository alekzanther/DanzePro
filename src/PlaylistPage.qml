import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import danzepro.playlist 1.0

Page {
    id: root
    property DanzePlaylist playlist: null
    Rectangle {
        anchors.fill: parent
        id: dropindicator
        color: "#EEE";
        opacity: 0;
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: {
                if (mouse.button === Qt.RightButton)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }
            onPressAndHold: {
                if (mouse.source === Qt.MouseEventNotSynthesized)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }

            Menu {
                id: contextMenu
                MenuItemWithMDI {
                    text: "Paste from clipboard"
                    mdiIcon: '\uf750'
                    onTriggered: {
                        playlist.addSongsFromClipboard();
                    }
                }

            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 8

        Label {
            Layout.fillHeight: false;
            text: playlist.name;
            id: header;
            font.pixelSize: 24;
            anchors.horizontalCenter: parent.horizontalCenter;
            height: 50;
        }

        ListView {
            Layout.fillHeight: true;
            Layout.fillWidth: true;
            /*anchors.top: header.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right*/
            id: view
            // set our model to the views model property
            model: playlist.songs
            delegate: Song {
                width: ListView.view.width
                canMoveDown: index+1 < playlist.songs.count
                canMoveUp: index > 0
                song: playlist.songs.get(index)
                onClicked: {
                    // make this delegate the current item
                    view.currentIndex = index
                    view.focus = false

                }

                onRemove: {
                    // remove the current entry from the model
                    playlist.songs.remove(index)
                }
                onMoveDown: {
                    playlist.songs.move(index, index+1)
                }
                onMoveUp: {
                    playlist.songs.move(index, index-1)

                }

            }
            /*highlight: Rectangle {
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#222" }
                    GradientStop { position: 1.0; color: "#111" }
                }
            }*/

            // some fun with transitions :-)
            add: Transition {
                // applied when entry is added
                NumberAnimation {
                    properties: "x"; from: -view.width;
                    duration: 150; easing.type: Easing.InCirc
                }
                NumberAnimation { properties: "y"; from: view.height;
                    duration: 150; easing.type: Easing.InCirc
                }
            }
//            move: Transition {
//                NumberAnimation { properties: "x,y"; duration: 1000 }
//            }

            remove: Transition {
                // applied when entry is removed
                NumberAnimation {
                    properties: "x"; to: view.width;
                    duration: 1500; easing.type: Easing.OutBounce
                }

                NumberAnimation {
                    property: "opacity"
                    to: 0
                    duration: 200
                    easing.type: Easing.OutSine
                }
            }
            displaced: Transition {
                // applied when entry is moved
                // (e.g because another element was removed)
                SequentialAnimation {
                    // wait until remove has finished
                    PauseAnimation { duration: 250 }
                    NumberAnimation { properties: "y"; duration: 75; easing.type: Easing.bezierCurve
                    }
                }
            }
        }


    }

    DropArea {
        anchors.fill: parent
        onEntered:
        {
            dropindicator.opacity = 0.5
        }
        onExited: {
            dropindicator.opacity = 0
        }
        onDropped: {
            dropindicator.opacity = 0
            if (drop.hasUrls) {
                if (drop.proposedAction == Qt.MoveAction || drop.proposedAction == Qt.CopyAction) {
                    playlist.addSongs(drop.urls);
                    drop.acceptProposedAction()
                }
            }
        }
    }
}
