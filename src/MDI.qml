import QtQuick 2.7


//checkout https://cdn.materialdesignicons.com/3.5.95/ for what to set text to
Text {
    id: root
    property alias size: root.font.pixelSize

    FontLoader { id: mdiFont; source: 'qrc:/materialdesignicons/fonts/materialdesignicons-webfont.ttf' }

    font.family: mdiFont.name
    font.pixelSize: 96
    opacity: 0.75
}
