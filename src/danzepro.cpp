#include "danzepro.h"
#include <QMessageBox>
#include <QKeySequence>
using namespace std;

namespace DanzeProApp {

DanzePro::DanzePro(QObject *parent)
    : QObject(parent)
{
    m_playlists = new QQmlObjectListModel<DanzePlaylist>(this);
    m_active_playlist = Q_NULLPTR;
    m_active_song = Q_NULLPTR;
    loadSettings();

    dlManager = new DownloadManager(this);
}

DanzePro::~DanzePro()
{
    saveSettings();
}

void DanzePro::addPlaylist(QString name, QSettings *settings)
{
    DanzePlaylist *playlist = new DanzePlaylist;
    playlist->set_name(name);
    m_playlists->append(playlist);
    if (m_active_playlist == Q_NULLPTR) {
        set_active_playlist(playlist);

    }

    playlist->danzePro = this;

    if (settings != Q_NULLPTR) {
        playlist->loadSettings(settings);
    }
}

void DanzePro::setPlaylist(DanzePlaylist *pl)
{
    set_active_playlist(pl);
}

void DanzePro::loadSettings()
{
    QSettings setting("danzepro", "app");
    setting.beginGroup("playlists");

    for (QString playlistName : setting.childGroups()) {
        setting.beginGroup(playlistName);

        addPlaylist(playlistName, &setting);

        setting.endGroup();
    }

    setting.endGroup();
}

void DanzePro::saveSettings()
{
    QSettings setting("danzepro", "app");

    setting.clear();
    setting.beginGroup("playlists");

    for (auto it = get_playlists()->begin(); it != get_playlists()->end(); ++it) {
        auto playlist = *it;
        playlist->saveSettings(&setting);
    }
    setting.endGroup();
}


void DanzePro::songPlaying(DanzeSong *song)
{
    if (m_active_song != song && m_active_song != Q_NULLPTR)  {
        if (m_active_song->get_is_playing()) {
            m_active_song->pauseSong();
        }
    }

    set_active_song(song);
}

void DanzePro::spacePressed()
{
    if (m_active_song != Q_NULLPTR) {
        if (m_active_song->get_is_playing()) {
            m_active_song->pauseSong();
        } else {
            m_active_song->rewindSong();
            m_active_song->playSong();
        }
    }

 }

}
