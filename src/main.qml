import QtQuick 2.7
import QtQuick.Controls 2.0

//import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import danzepro.model 1.0

ApplicationWindow {
    visible: true
    width: 720
    height: 480
    title: qsTr("DanzePro - your oasis on the dance floor")
    DanzePro {
        id: danzepro
    }

    Shortcut {
        sequence: "Space"
        onActivated: danzepro.spacePressed();
    }
    Shortcut {
        sequence: StandardKey.Paste
        onActivated: {
            if (danzepro.active_playlist != null) {
                danzepro.active_playlist.addSongsFromClipboard();
            }
        }
    }

    RowLayout {
        id: swipeView
        anchors.fill: parent

        Page {

            Layout.minimumWidth: parent.width * 0.3 < 250 ? 250 : parent.width * 0.2;
            Layout.fillHeight: true;
            background: Rectangle {
                id: kalle
                anchors.fill: parent
                color: "#222"
            }

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 0
                //ScrollView {
                    //Layout.fillHeight: true
                    //Layout.fillWidth: true
                    InputField {
                        anchors.top: parent.top
                        width: parent.width
                        height: 50
                        id: playlistInput
                        onAppend: {
                            // called when the user presses return on the text field
                            // or clicks the add button
                            danzepro.addPlaylist(name)

                        }

                        onUp: {
                            // called when the user presses up while the text field is focused
                            view.decrementCurrentIndex()
                        }
                        onDown: {
                            // same for down
                            view.incrementCurrentIndex()
                        }
                    }
                    ListView {
                        anchors.left: parent.left
                        anchors.right : parent.right
                        anchors.bottom: parent.bottom
                        anchors.top: playlistInput.bottom
                        highlightFollowsCurrentItem: true;
                        id: view
                        // set our model to the views model property
                        model: danzepro.playlists
                        delegate: PlaylistsListDelegate {
                            width: ListView.view.width
                            // construct a string based on the models proeprties
                            text: model.name
                            onClicked: {
                                // make this delegate the current item
                                view.currentIndex = index
                                view.focus = true

                                //temporary hack until I learn this properly... :(
                                danzepro.setPlaylist(danzepro.playlists.get(index))

                            }
                            onRemove: {
                                // remove the current entry from the model
                                danzepro.playlists.remove(index)
                            }
                            onFocusChanged: {
                                if (view.currentIndex == index) {
                                    textColor = "#f48fb1"
                                } else {
                                    textColor = "#fafafa"
                                }
                            }
                        }
                        highlight: Item {


                            Rectangle {
                                visible: (view.currentIndex == index)
                                anchors.fill: parent
                                color: "#303030"
                            }
                            Rectangle {
                                visible: (view.currentIndex == index)
                                color: "#f48fb1"
                                width: 5
                                height: parent.height
                                anchors.left: parent.left
                            }
                        }

                        // some fun with transitions :-)
                        add: Transition {
                            // applied when entry is added
                            NumberAnimation {
                                properties: "x"; from: -view.width;
                                duration: 250; easing.type: Easing.InCirc
                            }
                            NumberAnimation { properties: "y"; from: view.height;
                                duration: 250; easing.type: Easing.InCirc
                            }
                        }
                        remove: Transition {
                            // applied when entry is removed
                            NumberAnimation {
                                properties: "x"; to: view.width;
                                duration: 250; easing.type: Easing.InBounce
                            }
                        }
                        displaced: Transition {
                            // applied when entry is moved
                            // (e.g because another element was removed)
                            SequentialAnimation {
                                // wait until remove has finished
                                PauseAnimation { duration: 250 }
                                NumberAnimation { properties: "y"; duration: 75
                                }
                            }
                        }
                    }
            }

        }

        PlaylistPage {
            Layout.fillHeight: true
            Layout.fillWidth: true

            visible: danzepro.active_playlist != null
            playlist: danzepro.active_playlist
        }
    }

    footer: SongControls {
            visible: danzepro.active_song != null
            song: danzepro.active_song
    }

}
