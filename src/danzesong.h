#ifndef DANZESONG_H
#define DANZESONG_H


#include <QtCore>
#include <QtGui>
#include <QUuid>
#include <QMediaPlayer>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlAutoPropertyHelpers.h"
#include "QQmlObjectListModel.h"
#include "QQmlVariantListModel.h"



namespace DanzeProApp {

class DanzeSong : public QObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (QUrl, url)
    QML_WRITABLE_VAR_PROPERTY (QString, name)
    QML_WRITABLE_VAR_PROPERTY (qreal, position)
    QML_WRITABLE_VAR_PROPERTY (qreal, duration)
    QML_WRITABLE_VAR_PROPERTY (qreal, start)
    QML_WRITABLE_VAR_PROPERTY (qreal, end)
    QML_WRITABLE_VAR_PROPERTY (qreal, rate)
    QML_WRITABLE_VAR_PROPERTY (bool, media_ready)
    QML_WRITABLE_VAR_PROPERTY (bool, is_playing)
    QML_WRITABLE_VAR_PROPERTY (int, list_position)

public:
    explicit DanzeSong (QObject *parent = Q_NULLPTR);

    Q_INVOKABLE void toggleSong();
    Q_INVOKABLE void pauseSong();
    Q_INVOKABLE void playSong();
    Q_INVOKABLE void rewindSong();
    Q_INVOKABLE void duplicateSong();

signals:

    void songPlaying(DanzeSong *song);
    void duplicateSong(DanzeSong *song);

public slots:
    void mediaLoaded(QMediaPlayer::MediaStatus status);
    void playPositionChanged(qint64 pos);
    void setPlayPosition(qint64 pos);
    void setPlaybackRate(qreal pbrate);
    void downloadFinished();
private:

    friend class DanzePlaylist;

    void resolveMedia();
    void resolveMedia(QUrl url);
    void saveSettings(QSettings *settings);
    void loadSettings(QSettings *settings);
    QUuid guid;
    QMediaPlayer *player;



};

}

#endif // DANZESONG_H
