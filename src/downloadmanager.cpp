#include "downloadmanager.h"
#include <QFileInfo>
#include <QNetworkAccessManager>

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{

    m_can_download = false;
    if (!downloadToolsAvailable()) {
        attemptToDownloadTools();
    } else {
        m_can_download = true;
    }

    //Download("https://www.youtube.com/watch?v=HUHC9tYz8ik")->start();

}

QProcess *DownloadManager::Download(QString url, QString uid)
{
    QProcess *process = new QProcess(this);
    process->setProgram("./" + DOWNLOAD_TOOL);
    process->setArguments(QStringList() << "-o" << uid + "--%(title)s.%(ext)s" << "-f" << "bestaudio" <<  "--extract-audio" << "--audio-format" << "mp3" << "--audio-quality" << "5" << url);
    return process;

}

void DownloadManager::httpReadyRead()
{
    if (file)
        file->write(reply->readAll());
}

void DownloadManager::httpDownloadFinished()
{
    // when canceled
    if (httpRequestAborted) {
        if (file) {
            file->close();
            file->remove();
            delete file;
            file = 0;
        }
        reply->deleteLater();
        return;
    }

    // download finished normally
    file->flush();
    file->close();

    // get redirection url

    QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (reply->error()) {
        file->remove();
        qDebug() << "Tool download failed";

    } else if (!redirectionTarget.isNull()) {
        QUrl newUrl = QUrl(currentUrl).resolved(redirectionTarget.toUrl());
        qDebug() << "URL Apparently redirected... to << ";
        reply->deleteLater();
        file->open(QIODevice::WriteOnly);
        file->resize(0);
        startRequest(newUrl);
        return;

    } else {
        QString fileName = QFileInfo(QUrl(DOWNLOAD_TOOL_URL).path()).fileName();
        qDebug() << "Downloaded " << fileName << " to " << QDir::currentPath();
        file->setPermissions(QFile::WriteUser | QFile::ReadUser | QFile::WriteGroup | QFile::ReadGroup | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOwner);
    }

    reply->deleteLater();
    reply = 0;
    delete file;
    file = 0;
    manager = 0;

}

void DownloadManager::updateDownloadProgress(qint64, qint64)
{

}

void DownloadManager::startRequest(QUrl url)
{
    manager = new QNetworkAccessManager(this);
    currentUrl = url;
    reply = manager->get(QNetworkRequest(url));

    // Whenever more data is received from the network,
    // this readyRead() signal is emitted
    connect(reply, SIGNAL(readyRead()),
            this, SLOT(httpReadyRead()));

    // Also, downloadProgress() signal is emitted when data is received
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            this, SLOT(updateDownloadProgress(qint64,qint64)));

    // This signal is emitted when the reply has finished processing.
    // After this signal is emitted,
    // there will be no more updates to the reply's data or metadata.
    connect(reply, SIGNAL(finished()),
            this, SLOT(httpDownloadFinished()));


}

bool DownloadManager::downloadToolsAvailable()
{
    return QFileInfo::exists(DOWNLOAD_TOOL) && QFileInfo(DOWNLOAD_TOOL).isFile();
}

void DownloadManager::attemptToDownloadTools()
{
    //....
    manager = new QNetworkAccessManager(this);

    QFileInfo fileInfo(DOWNLOAD_TOOL);
    QString fileName = fileInfo.fileName();

    if (QFile::exists(fileName)) {
        qDebug() << "Download tool already exists?!";
        QFile::remove(fileName);
    }

    file = new QFile(fileName);
    if (!file->open(QIODevice::WriteOnly)) {
        qDebug() << "Unable to write to " << fileInfo.absoluteFilePath();
        delete file;
        file = 0;
        return;
    }

    httpRequestAborted = false;
    startRequest(QUrl(DOWNLOAD_TOOL_URL));

}


