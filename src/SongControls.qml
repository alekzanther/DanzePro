import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Universal 2.2
import QtQuick.Layouts 1.3
import danzepro.song 1.0;
import QtMultimedia 5.8;

Item {
    id: root
    Layout.fillWidth: true;
    height: 100
    property DanzeSong song: null

    signal clicked()
    signal remove()
    signal togglePlay()

    Rectangle {
        anchors.fill: root
        color: '#111'

    }
    Text {
        visible: !song.media_ready
        anchors.centerIn: root
        color: '#f7f7f7'
        font.pixelSize: 16
        text: "Loading... " + song.name
    }

    RowLayout {
        visible: song.media_ready
        anchors.fill: root //
        anchors.leftMargin: 8
        anchors.rightMargin: 8
        anchors.bottomMargin: 4
        anchors.topMargin: 4
        spacing: 8

        Item {
            height: parent.height
            width: height

            Rectangle {
                visible: false;
                color: "#dedede"
                height: parent.height
                width: height
            }

            MDI {
                visible: !song.is_playing
                id: playIcon
                anchors.centerIn: parent
                text: '\uf40d'
                size: 70
                color: 'white'
            }

            MDI {
                visible: song.is_playing
                id: pauseIcon
                anchors.centerIn: parent
                text: '\uf3e5'
                size: 70
                color: 'white'
            }
            MouseArea {
                anchors.fill: parent
                onClicked: song.toggleSong();
            }

        }

        ColumnLayout {

            Layout.fillWidth: true

            RowLayout {

                Layout.fillHeight: true
                Layout.fillWidth: true
                Item {

                    property bool editing: false
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        width: parent.width
                        clip: true
                        id: label
                        visible: !parent.editing
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        color: '#f7f7f7'
                        font.pixelSize: 16
                        text: song.name
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.editing = true
                            labelEdit.forceActiveFocus();
                        }
                    }
                    TextField {
                        width: label.width
                        anchors.verticalCenter: parent.verticalCenter
                        id: labelEdit
                        visible: parent.editing
                        text: song.name
                        onFocusChanged: {
                            if (!focus) {
                                accepted();
                            }
                        }

                        onAccepted: {
                            parent.editing = false;
                            song.name = text;
                        }
                    }
                }
                Item {
                    Layout.fillHeight: true;
                    Layout.minimumWidth: 200;

                    Layout.alignment: Qt.AlignRight;
                    SpinBox {
                        anchors.right: parent.right;
                        from: 50
                        to: 120
                        stepSize: 2
                        value: Math.round(song.rate * 100.0)

                        onValueChanged: {
                            song.rate = (value / 100.0)
                        }

                    }

                }


            }

            Item {
                //anchors.fill: parent
                Layout.fillHeight: true
                Layout.fillWidth: true
                Slider {
                    visible: song.media_ready
                    anchors.fill: parent
                    from: song.start
                    to: song.end
                    value: song.position
                    onMoved: {
                        song.setPlayPosition(value);
                    }
                }

            }


        }
    }

}
