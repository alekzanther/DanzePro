#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QtCore>
#include <QtGui>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlAutoPropertyHelpers.h"
#include "QQmlObjectListModel.h"
#include "QQmlVariantListModel.h"




class DownloadManager : public QObject
{
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (bool, can_download)
public:
    explicit DownloadManager(QObject *parent = nullptr);
    QProcess *Download(QString url, QString uid);

signals:

    void downloadFinished();

public slots:

    // slot for readyRead() signal
    void httpReadyRead();

    // slot for finished() signal from reply
    void httpDownloadFinished();

    // slot for downloadProgress()
    void updateDownloadProgress(qint64, qint64);

private:

    void startRequest(QUrl url);
    bool downloadToolsAvailable();
    void attemptToDownloadTools();

    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    QFile *file;
    bool httpRequestAborted;
    qint64 fileSize;

    QUrl currentUrl;
#ifdef Q_OS_LINUX
    const QString DOWNLOAD_TOOL_URL = "https://yt-dl.org/downloads/latest/youtube-dl";
    const QString DOWNLOAD_TOOL = "youtube-dl";
#elif defined(Q_OS_WIN32)
    const QString DOWNLOAD_TOOL_URL = "https://yt-dl.org/latest/youtube-dl.exe";
    const QString DOWNLOAD_TOOL = "youtube-dl.exe";
#endif


};

#endif // DOWNLOADMANAGER_H
