import QtQuick 2.7
import QtQuick.Controls 2.0

MenuItem {
    id: menuItem
    property string mdiIcon;
    property string mdiIconColor: "white";
    property string textColor: "white";
    indicator: Item {
        implicitWidth: 40
        //implicitHeight: 20
        anchors.verticalCenter: parent.verticalCenter


        MDI {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            size: 24
            text: mdiIcon
            color: mdiIconColor
        }
    }

    contentItem: Text {
        leftPadding: menuItem.indicator.width
        text: menuItem.text
        font: menuItem.font
        opacity: enabled ? 1.0 : 0.3
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        color: textColor
    }
}

