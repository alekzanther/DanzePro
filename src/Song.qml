import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Universal 2.2
import QtQuick.Layouts 1.3
import danzepro.song 1.0;
import QtMultimedia 5.8;

Item {
    id: songRoot
    width: ListView.view.width
    height: 100
    property DanzeSong song: null

    signal clicked()
    signal remove()
    signal moveUp()
    signal moveDown()

    property bool canMoveDown: true
    property bool canMoveUp: true

    Rectangle {
        id: backgroundContent
        anchors.fill: parent
        color: "#444"
        gradient: Gradient {
            GradientStop { position: 0.0; color: song.is_playing ? "#222" : "#444"}
            GradientStop { position: 1.0; color: song.is_playing ? "#111" : "#444" }
        }
        Behavior on color { ColorAnimation { duration: 100 } }
        //opacity: 0.2

        border.color: Qt.darker(color)
        MouseArea {

            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton

            onClicked: {
                if (mouse.button === Qt.RightButton)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }

            onPressAndHold: {
                held = true
                if (mouse.source === Qt.MouseEventNotSynthesized)
                {
                    contextMenu.x = mouse.x;
                    contextMenu.y = mouse.y;
                    contextMenu.open()
                }
            }

            Menu {
                id: contextMenu
                MenuItemWithMDI {
                    text: "Copy"
                    mdiIcon: '\uf18f'
                    onTriggered: {
                        song.duplicateSong();
                    }
                }
                MenuItemWithMDI {
                    text: "Delete"
                    mdiIcon: '\uf5e8'
                    mdiIconColor: "red"
                    onTriggered: {
                        songRoot.remove();
                    }
                }

            }
        }


    }
    Text {
        visible: !song.media_ready
        anchors.centerIn: parent
        color: '#f7f7f7'
        font.pixelSize: 16
        text: "Loading... " + song.name
    }
    RowLayout {
        visible: song.media_ready
        anchors.fill: parent //
        anchors.leftMargin: 8
        anchors.rightMargin: 8
        anchors.bottomMargin: 4
        anchors.topMargin: 4
        spacing: 8

        Item {
            height: parent.height
            width: height

            MDI {
                visible: !song.is_playing
                id: playIcon
                anchors.centerIn: parent
                text: '\uf40d'
                size: 70
                color: 'white'
            }

            MDI {
                visible: song.is_playing
                id: pauseIcon
                anchors.centerIn: parent
                text: '\uf3e5'
                size: 70
                color: 'white'
            }
            MouseArea {
                anchors.fill: parent
                onClicked: song.toggleSong();
            }

        }

        ColumnLayout {

            Layout.fillWidth: true

            RowLayout {

                Layout.fillHeight: true
                Layout.fillWidth: true
                Item {

                    property bool editing: false
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        width: parent.width
                        clip: true
                        id: label
                        visible: !parent.editing
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        color: '#f7f7f7'
                        font.pixelSize: 16
                        text: model.name
                        //text: model.rate
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.editing = true
                            labelEdit.forceActiveFocus();
                        }
                    }
                    TextField {
                        width: label.width
                        anchors.verticalCenter: parent.verticalCenter
                        id: labelEdit
                        visible: parent.editing
                        text: model.name
                        onFocusChanged: {
                            if (!focus) {
                                accepted();
                            }
                        }

                        onAccepted: {
                            parent.editing = false;
                            model.name = text;
                        }
                    }
                }

                Item {
                    Layout.fillHeight: true;
                    Layout.minimumWidth: 200;

                    Layout.alignment: Qt.AlignRight;
                    SpinBox {


                        anchors.right: parent.right;
                        from: 50
                        to: 120
                        stepSize: 2
                        value: Math.round(model.rate * 100.0)

                        onValueChanged: {
                            model.rate = (value / 100.0)
                        }

                    }

                }


            }

            Item {
                //anchors.fill: parent
                Layout.fillHeight: true
                Layout.fillWidth: true
                Slider {
                    visible: song.media_ready
                    anchors.fill: parent
                    from: 0
                    to: song.duration
                    value: song.position
                }

                RangeSlider {
                    Layout.fillHeight: true
                    visible: song.media_ready

                    anchors.fill: parent
                    from: 0
                    to: song.duration
                    second.value: song.end
                    first.value: song.start
                    first.onValueChanged: {
                        if (song.media_ready)  {
                            song.set_start(first.value);
                            //console.log("setting start: " + first.value.toString());
                        }
                    }
                    second.onValueChanged: {
                        if (song.media_ready)  {
                            song.set_end(second.value);
                            //console.log("setting end: " + second.value.toString());
                        }
                    }

                }
            }


        }
        ColumnLayout {
            height: parent.height
            Layout.fillWidth: false
            width: 40
            Item {
                Layout.fillHeight: true

                width: parent.width

                MDI {
                    id: upButtonGfx
                    anchors.centerIn: parent
                    text: '\uf05d'
                    size: 30
                    color: canMoveUp ? '#fff' : "#666"
                    opacity: 0.3
                }

                MouseArea {
                    id: upButton
                    hoverEnabled: true
                    enabled: canMoveUp
                    onHoveredChanged: {
                        if (upButton.containsMouse) {
                            upButtonGfx.opacity = 1
                        } else {
                            upButtonGfx.opacity = 0.3
                        }
                    }

                    anchors.fill: parent
                    onClicked: {
                        songRoot.moveUp();
                        upButtonGfx.opacity = 0.3;
                    }

                }

            }

            Item {

                Layout.fillHeight: true

                width: parent.width

                MDI {
                    id: downButtonGfx
                    anchors.centerIn: parent
                    opacity: 0.3
                    text: '\uf045'
                    size: 30
                    color: canMoveDown ? '#fff' : "#666"
                }

                MouseArea {
                    id: downButton
                    enabled: canMoveDown
                    hoverEnabled: true
                    onHoveredChanged: {
                        if (downButton.containsMouse) {
                            downButtonGfx.opacity = 1
                        } else {
                            downButtonGfx.opacity = 0.3
                        }
                    }

                    anchors.fill: parent
                    onClicked: {
                        songRoot.moveDown();
                        downButtonGfx.opacity = 0.3
                    }

                }

            }
        }



    }


}
