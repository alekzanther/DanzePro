#include "danzesong.h"

namespace DanzeProApp {


DanzeSong::DanzeSong(QObject *parent): QObject(parent)
{
    qDebug() << "awd";
    player = new QMediaPlayer(this);
    m_end = -1;
    set_media_ready(false);
    set_is_playing(false);
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(mediaLoaded(QMediaPlayer::MediaStatus)));

    set_position(0);
    set_rate(1);
    set_duration(0);

}

void DanzeSong::toggleSong()
{

    qDebug() << player->state() << " song " << get_url();
    if (player->state() == QMediaPlayer::PausedState || player->state() == QMediaPlayer::StoppedState) {


        playSong();
        emit songPlaying(this);

    } else {

        pauseSong();
    }

    if (player->errorString() != "")
    {
        qDebug() << "Something went wrong" << player->errorString();
    }
}

void DanzeSong::pauseSong()
{
    player->pause();
    set_is_playing(false);
}

void DanzeSong::playSong()
{
    player->play();
    set_is_playing(true);
}

void DanzeSong::rewindSong()
{

    player->setPosition(m_start);
    m_position = m_start;
}

void DanzeSong::duplicateSong()
{
    emit duplicateSong(this);
}

void DanzeSong::mediaLoaded(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::LoadedMedia) {

        set_duration(player->duration());

        if (m_rate == 0) {
            qDebug() << "read rate: " << m_rate;
            set_rate(player->playbackRate());
        } else {
            player->setPlaybackRate(m_rate);
        }

        player->setPosition(m_position);

        if (m_end <= 0) //) || get_end() > player->duration())
        {
            qDebug() << "read end: " << m_end;
            set_end(player->duration());
        }

        emit endChanged(m_end);
        emit startChanged(m_start);
        emit durationChanged(m_duration);
        emit positionChanged(m_position);
        emit rateChanged(m_rate);

        connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(playPositionChanged(qint64)));

        connect(this, SIGNAL(rateChanged(qreal)), this, SLOT(setPlaybackRate(qreal)));

        set_media_ready(true);

    } else {
        qDebug() << status;
    }
}

void DanzeSong::playPositionChanged(qint64 pos)
{
    if (m_is_playing && get_media_ready()) {
        if (pos < get_start() || pos > get_end()) {

            if (pos < get_start()) {
                player->setPosition(get_start());
            } else if (pos > get_end()) {
                player->setPosition(get_start());
            }

        }

        this->set_position(pos);
    }
}

void DanzeSong::setPlayPosition(qint64 pos)
{
    player->setPosition(pos);
}

void DanzeSong::setPlaybackRate(qreal pbrate)
{
    player->setPlaybackRate(pbrate);
}

void DanzeSong::downloadFinished()
{
    qDebug() << "Download finished...";
    auto dir = QDir();
    QString file = dir.entryList(QStringList() << this->guid.toString().remove("}").remove("{") + "*.mp3",QDir::Files)[0];

    set_url(QUrl::fromLocalFile(dir.absoluteFilePath(file)));
    set_name(file.remove(guid.toString().remove("}").remove("{")).remove("--").remove(".mp3"));
    resolveMedia();

}

void DanzeSong::saveSettings(QSettings *settings)
{
    settings->beginGroup(guid.toString());

    settings->setValue("url", get_url().toString());
    settings->setValue("name", get_name());
    settings->setValue("start", get_start());
    settings->setValue("end", get_end());
    settings->setValue("position", get_position());
    settings->setValue("rate", get_rate());
    settings->setValue("list_position", get_list_position());
    settings->endGroup();
}

void DanzeSong::loadSettings(QSettings *settings)
{
    set_url(settings->value("url").toUrl());
    set_name(settings->value("name").toString());

    m_start = settings->value("start").toReal();
    m_end = settings->value("end").toReal();
    m_position = settings->value("position").toReal();
    m_rate = settings->value("rate").toReal();
    m_list_position = settings->value("list_position").toInt();

    resolveMedia();
}

void DanzeSong::resolveMedia()
{
    resolveMedia(get_url());
}

void DanzeSong::resolveMedia(QUrl url)
{
    player->setMedia(url);
}


}
