#include "danzeplaylist.h"
#include "danzepro.h"
#include "QMediaPlayer"

namespace DanzeProApp {

void DanzePlaylist::duplicateSong(DanzeSong *source)
{
    DanzeSong *song = new DanzeSong(this);
    song->set_url(source->get_url());
    song->set_duration(source->get_duration());
    song->set_end(source->get_end());
    song->set_start(source->get_start());
    song->set_rate(source->get_rate());
    song->set_position(source->get_position());
    song->set_name(source->get_name() + " (Copy)");
    song->guid = QUuid::createUuid();
    song->resolveMedia();
    connectSignals(song);
    m_songs->insert(m_songs->indexOf(source) +1, song);

}

void DanzePlaylist::addSong(const QUrl &url)
{
    if (url.scheme().contains("http")) {
        if (danzePro->dlManager->get_can_download()) {
            DanzeSong *song = new DanzeSong(this);
            song->set_url(QUrl(url));
            song->set_name(url.host());
            song->guid = QUuid::createUuid();
            m_songs->append(song);
            connectSignals(song);

            auto dlProcess = danzePro->dlManager->Download(url.toString(), song->guid.toString().remove("}").remove("{"));
            connect(dlProcess, SIGNAL(finished(int)), song, SLOT(downloadFinished()));
            dlProcess->start();
        } else {
            //LOG/NOTIFY of issues with download tool
        }
    } else {
        DanzeSong *song = new DanzeSong(this);
        song->set_url(QUrl(url));
        song->set_name(url.fileName());
        song->guid = QUuid::createUuid();
        song->resolveMedia();
        m_songs->append(song);
        connectSignals(song);
    }

}

void DanzePlaylist::addSongs(const QList<QUrl> &urls)
{
    for (const QUrl &url : urls) {
        addSong(url);
    }
}

void DanzePlaylist::addSongsFromClipboard()
{
    addSong(QGuiApplication::clipboard()->text());
}

void DanzePlaylist::saveSettings(QSettings *settings)
{
    settings->beginGroup(get_name());

    int index = 0;
    for (auto it = get_songs()->begin(); it != get_songs()->end(); ++it, index++) {
        auto song = *it;
        song->set_list_position(index);
        song->saveSettings(settings);

    }

    settings->endGroup();

}

void DanzePlaylist::loadSettings(QSettings *settings)
{
    auto unorderedList = new std::vector<DanzeSong *>();

    for (QString songUuid : settings->childGroups()) {
        settings->beginGroup(songUuid);

        DanzeSong *song = new DanzeSong;
        song->guid = QUuid(songUuid);
        song->loadSettings(settings);
        unorderedList->push_back(song);

        settings->endGroup();
    }

    std::sort(unorderedList->begin(), unorderedList->end(), [](DanzeSong *a, DanzeSong *b) {
       return a->get_list_position() < b->get_list_position();
    });

    for (auto it = unorderedList->begin(); it != unorderedList->end(); ++it) {
        auto song = *it;
        m_songs->append(song);
        connectSignals(song);
    }

    delete unorderedList;

}


void DanzePlaylist::connectSignals(DanzeSong *song)
{
    connect(song, SIGNAL(songPlaying(DanzeSong*)), danzePro, SLOT(songPlaying(DanzeSong*)));
    connect(song, SIGNAL(duplicateSong(DanzeSong*)), this, SLOT(duplicateSong(DanzeSong*)));
}

}
