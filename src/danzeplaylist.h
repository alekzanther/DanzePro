#ifndef DANZEPLAYLIST_H
#define DANZEPLAYLIST_H


#include <QtCore>
#include <QtGui>
#include <QSettings>
#include <QUuid>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlAutoPropertyHelpers.h"
#include "QQmlObjectListModel.h"
#include "QQmlVariantListModel.h"
#include "danzesong.h"

namespace DanzeProApp {

class DanzePro;

class DanzePlaylist : public QObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY    (QString, name)
    QML_OBJMODEL_PROPERTY (DanzeSong, songs)
public:
    explicit DanzePlaylist (QObject *parent = Q_NULLPTR) : QObject(parent) {
         m_songs = new QQmlObjectListModel<DanzeSong>(this);
    }

    Q_INVOKABLE void addSong(const QUrl &url);
    Q_INVOKABLE void addSongs(const QList<QUrl> &urls);
    Q_INVOKABLE void addSongsFromClipboard();

public slots:

    void duplicateSong(DanzeSong *source);

private:
    friend class DanzePro;
    void saveSettings(QSettings *settings);
    void loadSettings(QSettings *settings);

    void connectSignals(DanzeSong *song);

    DanzePro *danzePro;

};

}

#endif // DANZEPLAYLIST_H
