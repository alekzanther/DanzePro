import QtQuick 2.7
import QtQuick.Controls 2.0
//import QtQuick.Window 2.3
import QtQuick.Layouts 1.3

Item {
    id: root
    signal append(string name);
    signal up();
    signal down();

    RowLayout {


        TextField {
            id: playlistName
            Layout.fillWidth: true
            text: 'test'
            focus: true
            onAccepted: {
                root.append(playlistName.text)
                selectAll()
            }
            Keys.onUpPressed: root.up()
            Keys.onDownPressed: root.down()
        }
        Button {
            text: 'Add'
            onClicked: {
                root.append(playlistName.text)
            }
        }
    }
}
