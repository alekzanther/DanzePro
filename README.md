# DanzePro

Qt based dance instructor program

## Downloads

The build system automatically produces (hopefully) runnable executables for each branch.

### Latest release

[Windows executable (master/stable version)](https://gitlab.com/alekzanther/DanzePro/-/jobs/artifacts/master/download?job=windows_build)

### Develop (unstable)

[Windows executable (develop version)](https://gitlab.com/alekzanther/DanzePro/-/jobs/artifacts/develop/download?job=windows_build)
